package shibascripts.mask;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.inject.Inject;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.yaml.YAMLConfigurationLoader;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.GameReloadEvent;
import org.spongepowered.api.event.game.state.GameLoadCompleteEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.plugin.Plugin;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.spongepowered.api.text.Text;
import shibascripts.mask.commands.MaskReloadCommand;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Plugin(
        id = "mask",
        name = "Mask",
        description = "A plugin used to detect users of VPNs and cloud services to connect to your server",
        url = "https://gitlab.com/shibascripts/mask",
        authors = {
                "nichoalsrobertm"
        }
)
public class Mask {

    public static Mask instance;

    @Inject
    private Logger logger;

    private YAMLConfigurationLoader loader;

    @Inject
    @ConfigDir(sharedRoot = false)
    private Path privateConfigDir;

    private ConfigurationNode rootNode;

    private OkHttpClient client = new OkHttpClient();
    private String apiKey;

    private JsonParser parser = new JsonParser();

    @Listener
    public void onGameLoaded(GameLoadCompleteEvent event) {
        instance = this;
        registerCommands();
        loader = YAMLConfigurationLoader.builder().setPath(Paths.get(privateConfigDir + ".yml")).build();
        reload();
        apiKey = Mask.getInstance().getRootNode().getNode("apiKey").getString();
    }

    @Listener
    public void onServerReload(GameReloadEvent event)
    {
        reload();
    }

    @Listener
    public void onClientConnectionEvent(ClientConnectionEvent.Join event,@Root Player player)
    {
        if (!player.hasPermission("mask.ignoreplayer"))
        {
            String resp = getPlayerIP(player);
            JsonObject jObject = (JsonObject) parser.parse(resp);
            Integer block = Integer.parseInt(String.valueOf(jObject.get("block")));


            Boolean executeCommand = Mask.getInstance().getRootNode().getNode("modes").getNode(block).getNode("executeCommands").getBoolean();;

            List<String> commands = new ArrayList<>();

            Mask.getInstance().getRootNode().getNode("modes").getNode(block).getNode("commands").getChildrenMap().forEach((id, command) ->
            {
                commands.add((String) convertOutput((String) command.getValue(),player,jObject));
            });

            logger.warn(convertOutput("{player} has logged in from {ip} with asn: {asn} at location {countryCode} {countryName} {isp} with block mode: {block}",player,jObject));

            if(executeCommand)
            {
                for (int i = 0; i < commands.size(); i++) {
                    logger.warn("Executing Command: " + String.valueOf(commands.get(i)));
                    Sponge.getCommandManager().process(Sponge.getServer().getConsole(), String.valueOf(commands.get(i)));
                }
            }


        }

    }

    private String getPlayerIP(@NotNull Player player)
    {
        Response response = null;
        String responseString = null;
        String playerIP = String.valueOf(player.getConnection().getAddress().getAddress());

        // For some reason they return an IP with a / infront of it.
        if(playerIP.contains("/")){
            playerIP = playerIP.replace("/", "");
        }

        String stringUrl = "http://v2.api.iphub.info/ip/" + playerIP;


        logger.debug("Making request to: " + stringUrl);
        Request request = new Request.Builder()
                .url(stringUrl)
                .header("X-Key", apiKey)
                .build();
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            responseString = response.body().string();
            logger.debug("Response received from server: " + responseString);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return responseString;
    }

    private String convertOutput(String unprocessedString, Player player, JsonObject jObject){
        String name = player.getName();
        String uuid = player.getUniqueId().toString();
        String processedString = unprocessedString;
        String ip = jObject.get("ip").toString();
        String countryCode = String.valueOf(jObject.get("countryCode"));
        String countryName = String.valueOf(jObject.get("countryName"));
        String asn = String.valueOf(jObject.get("asn"));
        String isp = jObject.get("isp").toString();
        Integer block = Integer.parseInt(String.valueOf(jObject.get("block")));


        if(processedString.contains("{player}")){
            processedString = processedString.replace("{player}", name);
        }
        if(processedString.contains("{uuid}")){
            processedString = processedString.replace("{uuid}", uuid);
        }
        if(processedString.contains("{ip}")){
            processedString = processedString.replace("{ip}", ip);
        }
        if(processedString.contains("{countryCode}")){
            processedString = processedString.replace("{countryCode}", countryCode);
        }
        if(processedString.contains("{countryName}")){
            processedString = processedString.replace("{countryName}", countryName);
        }
        if(processedString.contains("{asn}")){
            processedString = processedString.replace("{asn}", asn);
        }
        if(processedString.contains("{isp}")){
            processedString = processedString.replace("{isp}", isp);
        }
        if(processedString.contains("{block}")){
            processedString = processedString.replace("{block}", String.valueOf(block));
        }

        return processedString;
    }


    public void registerCommands() {

        CommandSpec maskReloadSpec = CommandSpec.builder()
                .description(Text.of("Reloads the plugin"))
                .permission("mask.admin.command.reload")
                .executor(new MaskReloadCommand())
                .build();

        CommandSpec maskCommandSpec = CommandSpec.builder()
                .permission("mask.command.user.base")
                .description(Text.of("Security All In one permission. Should only be given to admins"))
                .extendedDescription(Text.of("Usage:\n" +
                        "Reload's the configuration," +
                        "reload"))
                .child(maskReloadSpec, "reload", "r")
                .build();

        Sponge.getCommandManager().register(instance, maskCommandSpec, "mask", "mk");

    }

    public void loadConfig() {

        try{
            if(!Paths.get(privateConfigDir + ".yml").toFile().exists()){
                Paths.get(privateConfigDir + ".yml").toFile().createNewFile();
                PrintWriter pw = new PrintWriter(Paths.get(privateConfigDir + ".yml").toFile());
                pw.println("apiKey: yourkey # Usage: Put your api key to iphub.info in\n" +
                        "modes:\n" +
                        "  0: # Residential\n" +
                        "    executeCommands: false\n" +
                        "    commands: \n" +
                        "       commandOne: \"give {player} minecraft:cobblestone 1\"\n" +
                        "       commandTwo: \"give {player} minecraft:cobblestone 1\"\n" +
                        "  1: # Non-Residential\n" +
                        "    executeCommands: true\n" +
                        "    commands: \n" +
                        "       commandOne: \"give {player} minecraft:cobblestone 1\"\n" +
                        "       commandTwo: \"{player} with UUID: {uuid} has logged in from {ip} with asn: {asn} at location {countryCode} {countryName} {isp} with block mode: {block}\"\n" +
                        "  2: # Non-residential & Residential (Warning, may flag innocent people)\n" +
                        "    executeCommands: true\n" +
                        "    commands: \n" +
                        "       commandOne: \"give {player} minecraft:cobblestone 1\"\n" +
                        "       commandTwo: \"{player} with UUID: {uuid} has logged in from {ip} with asn: {asn} at location {countryCode} {countryName} {isp} with block mode: {block}\"");
                pw.close();
            }
            rootNode = loader.load();
            loader.save(rootNode);
        }catch(Exception e)
        {
            logger.error("Problem creating the config");
            logger.error(String.valueOf(e));
        }


    }
    public void reload() {
        loadConfig();
        apiKey = Mask.getInstance().getRootNode().getNode("apiKey").getString();
    }
    public static synchronized Mask getInstance() {
        return instance;
    }
    public ConfigurationNode getRootNode() {
        return rootNode;
    }

}
