Mask
==========

Mask is a plugin used to run commands against users of different IP levels. These levels include:
- Residential
- Non-Residential (VPN, Cloud Provider, etc)
- Mixed(If an IP is marked both residential / Non - Residential)

The primary use of this plugin is to prevent abuse of VPN clients to ban-evade on the server. 
Because you can run any commands against people in the different groups, you can:
- Log when VPN users access the server
- Combine with things like a discord link to message in your server discord when 
- Remove their perms and put them into a 'greylist' mode which requires a staff member to interact with them for them to play.
- Prevent them from logging into the server via a VPN at all with kick / ban commands
- Jail / Teleport them

Setup
=====
This plugin uses https://iphub.info for ip data collection. You'll need to create an account there and get an apikey to use this plugin. They have a free tier of 1000 Queries per day(Or 1000 logins in other words).

Permissions
===========
mask.admin.command.reload - Reload the config (and api key) in game
mask.command.user.base - Shows /mask usage
mask.ignoreplayer - ignores running check against a player with this permission

Command Usage
=============
* /mask reload - Reloads the GiveRandom config

Config
======
This plugin uses a [YAML](https://en.wikipedia.org/wiki/YAML) configuration file. 
To ensure the yaml is valid you can put it through any yaml linter, such as [this one](http://www.yamllint.com/).
This is formatted like the below:

apiKey: yourapikeyhere
modes:
    0:
        executeCommands: true
        commands: {commandOne: 'broadcast {player} logged in', commandTwo: 'give {player}
                minecraft:stone'}
    1:
        executeCommands: true
        commands: {commandOne: 'broadcast {player} logged in', commandTwo: 'give {player}
                minecraft:stone'}
    2:
        executeCommands: true
        commands: {commandOne: 'broadcast {player} logged in', commandTwo: 'give {player}
                minecraft:stone'}

Available replacements include:
    - {player} - The player name
    - {ip} - The player's ip address
    - {countryCode} - The country code received of the player
    - {countryName} - The country name received of the player
    - {asn} - The ASN received of the player
    - {isp} - The ISP received of the player
    - {mode} - The block mode received of the player